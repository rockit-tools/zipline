package net.brinkervii.rockit.zipline;

import lombok.Getter;

import java.util.HashMap;
import java.util.UUID;

public class Session {
	private final HashMap<String, Object> attributes = new HashMap<>();
	@Getter
	private final String id = UUID.randomUUID().toString();

	public <PT> PT setAttribute(String key, PT value) {
		this.attributes.put(key, value);
		return value;
	}
}
