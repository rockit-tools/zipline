package net.brinkervii.rockit.zipline.cli;

import net.brinkervii.rockit.zipline.State;
import net.brinkervii.rockit.zipline.ZiplineConfiguration;
import net.brinkervii.rockit.zipline.web.ZiplineServer;
import picocli.CommandLine;

import java.io.File;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "serve", description = "Start a Zipline synchronization server")
public class CLIServe implements Callable<Void> {
	private ZiplineServer server = null;

	@CommandLine.Option(names = {"-p", "--port"})
	private short port = -1;

	@Override
	public Void call() throws Exception {
		final ZiplineConfiguration configuration = State.current().loadConfiguration();

		short real_port;
		if (port < 0) {
			real_port = configuration.getPort();
		} else {
			real_port = port;
		}

		final File serveDir = new File(configuration.getWatchDir()).toPath().toAbsolutePath().normalize().toFile();
		State.current().setCwd(serveDir);

		this.server = new ZiplineServer(real_port);
		server.setEnableFileWatcher(true);
		Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

		server.start();

		return null;
	}

	private void shutdown() {
		if (this.server != null) {
			try {
				this.server.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
