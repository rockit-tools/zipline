package net.brinkervii.rockit.zipline.data;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.HashSet;
import java.util.Optional;

@Slf4j
public class ClientHolder {
	private final static ClientHolder instance = new ClientHolder();

	public static ClientHolder getInstance() {
		return instance;
	}

	@Getter
	private final HashSet<Client> clients = new HashSet<>();
	private final HashSet<ClientAddedListener> clientAddedListeners = new HashSet<>();

	public void addClientAddedListener(ClientAddedListener listener) {
		clientAddedListeners.add(listener);
	}

	public Client create(String sessionId, String name) {
		for (Client client : clients) {
			if (client.getSessionId().equals(sessionId)) return client;
		}

		final Client client = new Client(sessionId);
		client.setName(name);
		clients.add(client);

		clientAddedListeners.forEach(listener -> listener.onClientAdded(client));

		log.info("Added client with name " + client.getName());

		return client;
	}

	public void deleteClient(Client client) {
		clients.stream()
				.filter(c -> c.getSessionId().equals(client.getSessionId()))
				.forEach(clients::remove);

		log.info("Deleted client with name " + client.getName());
	}

	public Optional<Client> findClientById(String id) {
		return clients.stream()
				.filter(c -> c.getSessionId().equals(id))
				.findFirst();
	}

	public void addChange(Path path, WatchEvent.Kind<?> kind) {
		clients.forEach(client -> client.getChangeList().add(path, kind));
	}

	public interface ClientAddedListener {
		void onClientAdded(Client client);
	}
}
