package net.brinkervii.rockit.zipline.data;

import lombok.Data;
import net.brinkervii.rockit.zipline.ExceptionFunnel;
import net.brinkervii.rockit.zipline.web.model.CreatePackageRequest;
import net.brinkervii.rockit.zipline.web.model.DeletePackageRequest;

import java.io.IOException;
import java.time.LocalTime;
import java.util.LinkedHashSet;

@Data
public class Client {
	private final LocalTime timeCreated = LocalTime.now();
	private final String sessionId;
	private final ChangeList changeList = new ChangeList();
	private final LinkedHashSet<ZiplinePackage> ziplinePackages = new LinkedHashSet<>();
	private String name;

	public Client(String sessionId) {
		this.sessionId = sessionId;
	}

	public ZiplinePackage addPackage(CreatePackageRequest createPackageRequest) {
		final ZiplinePackage ziplinePackage = new ZiplinePackage();
		for (ChangeListEntry change : changeList.getChanges()) {
			for (String requestedChange : createPackageRequest.getChanges()) {
				if (requestedChange.equals(change.getId())) {
					ziplinePackage.getChanges().add(change);
				}
			}
		}

		try {
			ziplinePackage.loadFiles();
		} catch (IOException e) {
			ExceptionFunnel.accept(e);
		}

		synchronized (ziplinePackages) {
			ziplinePackages.add(ziplinePackage);
		}

		return ziplinePackage;
	}

	public void deletePackage(DeletePackageRequest deletePackageRequest) {
		synchronized (ziplinePackages) {
			final LinkedHashSet<ZiplinePackage> rubbish = new LinkedHashSet<>();

			for (ZiplinePackage ziplinePackage : ziplinePackages) {
				if (ziplinePackage.getId().equals(deletePackageRequest.getPackageId())) {
					rubbish.add(ziplinePackage);
				}
			}

			rubbish.forEach(ziplinePackages::remove);
		}
	}

	public ZiplinePackage getPackage(String id) {
		synchronized (ziplinePackages) {
			for (ZiplinePackage ziplinePackage : ziplinePackages) {
				if (ziplinePackage.getId().equals(id)) return ziplinePackage;
			}
		}

		return null;
	}

	public int changesLeft() {
		synchronized (changeList) {
			return changeList.size();
		}
	}
}
