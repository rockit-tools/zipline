package net.brinkervii.rockit.zipline.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import net.brinkervii.rockit.zipline.ExceptionFunnel;
import net.brinkervii.rockit.zipline.web.model.ZiplinePackageTakeout;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static java.nio.charset.StandardCharsets.UTF_8;

@Data
@JsonSerialize
public class ZiplinePackage {
	private String id = UUID.randomUUID().toString();

	@JsonIgnore
	private final ArrayList<ChangeListEntry> changes = new ArrayList<>();
	@JsonIgnore
	private Thread loadFilesThread;
	@JsonIgnore
	private AtomicBoolean loadComplete = new AtomicBoolean(false);
	@JsonIgnore
	private AtomicReference<Map<ChangeListEntry, String>> fileContents = new AtomicReference<>(new HashMap<>());

	public void loadFiles() throws IOException {
		if (this.loadFilesThread != null) throw new IOException();

		this.loadFilesThread = new Thread(() -> {
			try {
				this.loadFilesThreadFunc();
			} catch (IOException e) {
				ExceptionFunnel.accept(e);
			}
		});
		loadFilesThread.start();
	}

	private void loadFilesThreadFunc() throws IOException {
		loadComplete.set(false);

		try {
			for (ChangeListEntry change : changes) {
				if (!change.getFile().exists() || !change.getFile().isFile()) {
					fileContents.get().put(change, "");
					continue;
				}

				try (InputStream inputStream = new FileInputStream(change.getFile())) {
					fileContents.get().put(change, IOUtils.toString(inputStream, UTF_8));
				}
			}
		} catch (IOException e) {
			loadComplete.set(true);
			throw e;
		}

		loadComplete.set(true);
		this.loadFilesThread = null;
	}

	public ZiplinePackageTakeout take(int sizeLimit) {
		if (!this.loadComplete.get()) {
			while (!this.loadComplete.get()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					ExceptionFunnel.accept(e);
				}
			}
		}

		final ZiplinePackageTakeout takeout = new ZiplinePackageTakeout();

		final LinkedHashSet<ChangeListEntry> rubbish = new LinkedHashSet<>();
		for (ChangeListEntry change : changes) {
			takeout.put(change, fileContents.get().get(change));
			rubbish.add(change);

			if (takeout.calculateSize() >= sizeLimit) break;
		}

		takeout.setRemainingChanges(this.changes.size() - rubbish.size());
		rubbish.forEach(changes::remove);
		takeout.setPackageSize(takeout.getContent().size());

		return takeout;
	}
}
