package net.brinkervii.rockit.zipline.web.config;

import net.brinkervii.rockit.zipline.data.Client;
import net.brinkervii.rockit.zipline.data.ClientFactory;
import net.brinkervii.rockit.zipline.web.controller.AnnounceController;
import net.brinkervii.rockit.zipline.web.controller.ChangeListController;
import net.brinkervii.rockit.zipline.web.controller.PackageController;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;

import java.util.Arrays;
import java.util.HashSet;

public class ApplicationConfig extends ResourceConfig {
	public ApplicationConfig() {
		super(new HashSet<>(Arrays.asList(
				AnnounceController.class,
				ChangeListController.class,
				PackageController.class
		)));

		register(MarshallingFeature.class);

		register(new AbstractBinder() {
			@Override
			protected void configure() {
				bindFactory(ClientFactory.class).to(Client.class).in(RequestScoped.class);
			}
		});
	}
}
