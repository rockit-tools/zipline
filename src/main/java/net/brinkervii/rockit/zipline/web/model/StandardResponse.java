package net.brinkervii.rockit.zipline.web.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class StandardResponse {
	private final String message;

	public StandardResponse() {
		this.message = "OK";
	}
}
