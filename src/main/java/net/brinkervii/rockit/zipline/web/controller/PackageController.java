package net.brinkervii.rockit.zipline.web.controller;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.zipline.data.Client;
import net.brinkervii.rockit.zipline.data.ZiplinePackage;
import net.brinkervii.rockit.zipline.web.model.CreatePackageRequest;
import net.brinkervii.rockit.zipline.web.model.DeletePackageRequest;
import net.brinkervii.rockit.zipline.web.model.StandardResponse;
import net.brinkervii.rockit.zipline.web.model.ZiplinePackageTakeout;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Slf4j
@Path("package")
public class PackageController {
	@Inject
	Client client;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ZiplinePackageTakeout takeout(@QueryParam("id") String id) {
		final ZiplinePackage ziplinePackage = client.getPackage(id);
		if (ziplinePackage == null) return null;

		final ZiplinePackageTakeout takeout = ziplinePackage.take(512000);

		log.info(String.format("Client '%s' has %d changes left", client.getName(), client.changesLeft()));

		return takeout;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public ZiplinePackage createPackage(CreatePackageRequest createPackageRequest) {
		return client.addPackage(createPackageRequest);
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public StandardResponse deletePackage(DeletePackageRequest deletePackageRequest) {
		client.deletePackage(deletePackageRequest);

		log.info(String.format("Client '%s' has %d changes left", client.getName(), client.changesLeft()));

		return new StandardResponse();
	}
}
