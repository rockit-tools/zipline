package net.brinkervii.rockit.zipline.web.controller;

import net.brinkervii.rockit.zipline.data.ChangeListEntry;
import net.brinkervii.rockit.zipline.data.Client;
import net.brinkervii.rockit.zipline.web.model.ChangeDeleteRequest;
import net.brinkervii.rockit.zipline.web.model.StandardResponse;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("changelist")
public class ChangeListController {
	@Inject
	Client client;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<ChangeListEntry> getChanges() {
		return client.getChangeList().getChanges();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public StandardResponse deleteChanges(ChangeDeleteRequest deleteRequest) {
		client.getChangeList().deleteChanges(deleteRequest.getChanges());
		return new StandardResponse();
	}
}
