package net.brinkervii.rockit.zipline.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnnouncementResponse {
	private final String sessionId;
}
