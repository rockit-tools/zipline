package net.brinkervii;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.zipline.cli.CLIConfig;
import net.brinkervii.rockit.zipline.cli.CLIServe;
import picocli.CommandLine;

@Slf4j
@CommandLine.Command(
		name = "Zipline",
		description = "Synchronize files with Roblox Studio",
		subcommands = {CLIConfig.class, CLIServe.class}
)
public class Zipline implements Runnable {
	public static void main(String[] arguments) {
		CommandLine.run(new Zipline(), arguments);
	}

	@Override
	public void run() {
		CommandLine.usage(this, System.out);
	}
}
